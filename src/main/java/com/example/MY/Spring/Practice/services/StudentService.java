package com.example.MY.Spring.Practice.services;


import com.example.MY.Spring.Practice.student.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Service  //@component could also be use for service but service is more specific for identification
public class StudentService {

/*    StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }*/

    public List<Student> getStudents(){
        System.out.println("getStudents guys");
        return List.of(new Student(1l,"idrees","id@gmail.com",
                LocalDate.of(2000, Month.AUGUST,7),23));
    }


}
