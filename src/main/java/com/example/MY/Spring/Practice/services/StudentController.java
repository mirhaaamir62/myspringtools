package com.example.MY.Spring.Practice.services;


import com.example.MY.Spring.Practice.HttpUtils;
import com.example.MY.Spring.Practice.student.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/students")
public class StudentController {


    StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
        System.out.println("controller guys");
    }

    @GetMapping(value = "/getAll")
    public List<Student> getAllStudents() {
        System.out.println("getAllStudents guys");
//        return "Hello World";
        return studentService.getStudents();
    }

    @GetMapping(value = "/getRobot", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getRobot() {
        return HttpUtils.ok("ontologyDifferenceFinder.getIsSame(v1,v2)");
    }


}
